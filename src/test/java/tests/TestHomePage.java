package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

/**
 * Created by IlyaMakarov on 12/26/2016.
 */
public class TestHomePage {

    private final static String LOGIN_NAME = "IlyaMakarov";
    private final static String PASSWORD = "Password1";

    @Test
    public void logoutTest() {
        LoginPage loginPage = new LoginPage();
        HomePage homePage = loginPage.login(LOGIN_NAME, PASSWORD);
        loginPage = homePage.signOut();
        Assert.assertTrue(loginPage.isSubmitButtonDisplayed());
    }
}
