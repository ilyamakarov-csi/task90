package tests;


import org.testng.Assert;
import org.testng.annotations.Test;
import pages.HomePage;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

/**
 * Created by IlyaMakarov on 12/26/2016.
 */
public class TestLoginPage {

    private final static String LOGIN_NAME = "IlyaMakarov";
    private final static String PASSWORD = "Password1";

    @Test
    public void loginTest() throws InterruptedException {
        LoginPage loginPage = new LoginPage();
        TimeUnit.SECONDS.sleep(1000);
        HomePage homePage = loginPage.login(LOGIN_NAME, PASSWORD);
        Assert.assertTrue(homePage.isSignOutButtonVisible());
    }
}
