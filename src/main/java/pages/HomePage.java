package pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

/**
 * Created by IlyaMakarov on 12/26/2016.
 */
public class HomePage {

    private final static By SIGNOUT_LINK = By.cssSelector(".sign-out-span>a");

    public LoginPage signOut() {
        $(SIGNOUT_LINK).click();
        return new LoginPage();
    }

    public boolean isSignOutButtonVisible() {
        return $(SIGNOUT_LINK).is(Condition.visible);
    }
}
