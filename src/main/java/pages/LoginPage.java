package pages;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;


/**
 * Created by IlyaMakarov on 12/26/2016.
 */
public class LoginPage {
    private final static String ADDRESS = "https://192.168.100.26/";
    private final static By USERNAME_INPUT = By.id("Username");
    private final static By PASSWORD_INPUT = By.id("Password");
    private final static By SUBMIT_BUTTON = By.id("SubmitButton");

    public LoginPage() {
        open(ADDRESS);
    }

    public HomePage login(String login, String password) {
        $(USERNAME_INPUT).setValue(login);
        $(PASSWORD_INPUT).setValue(password);
        $(SUBMIT_BUTTON).click();

        return new HomePage();
    }

    public boolean isSubmitButtonDisplayed() {
        return $(SUBMIT_BUTTON).is(Condition.visible);
    }
}
